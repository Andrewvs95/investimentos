<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\User;
use App\Admin;
use App\Transaction;
use App\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ctrlAccountEdit extends Controller
{
    public function delete ($id) {
        $check = Auth::guard('admin')->check();

        if($check === true){
            $user = User::find($id);

            if(isset($user)){
                $user->delete();
            }

        return redirect('/admin/home');
        }

    return redirect()->route('login');
        
    }

    public function editForm ($id) {
        $check = Auth::guard('admin')->check();

        if($check === true){

            $user = User::find($id);
           
            if(isset($user)){
                return view('/admin/editForm', compact('user') );
            }
    
            return redirect('/admin/home');
        }

        return redirect()->route('login');

    }

    public function update (Request $request, $id) {
        
        $user = User::find($id);
        $wallet = Wallet::find($id);
        $historics = new Transaction();

        $user->name = $request->input('name');
        $user->surname = $request->input('surname');
        $user->email = $request->input('email');
        $user->gender = $request->input('gender');
        
        $historics->user_id =  $user->id;       
        $historics->balance_bf = $wallet->balance;
        $historics->balance_desc = 'credito adicionado';  
        $historics->balance += $request->input('wallet_balance');  

        $wallet->balance += $request->input('wallet_balance');  
        $historics->date = $historics->update_at;

        $historics->save();
        $wallet->save();
        $user->save();

        return redirect('/admin/home')->with('user', $user);

    }
}
