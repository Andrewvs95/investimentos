<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ctrlAdmin extends Controller
{
    public function index() {
        $check = Auth::guard('admin')->check();

        if($check === true){
            $users = User::all();
            $posts = Post::all();
            return view('admin.home', compact (['posts'],['users']) );
        }

        return redirect()->route('login');
        
    }

    public function loginForm() {
        return view('admin.loginForm');
    }

    public function login(Request $request){
        if(!filter_var($request->email, FILTER_VALIDATE_EMAIL)){
            return redirect()->back()->withInput()->withErrors(['Email informado pode esta incorreto']);
        }

        $request->validate([
            'email' => 'required|email|max:255',
            'password' => 'required|max:255',
        ]);

        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
        ]; 

        if(Auth::guard('admin')->attempt($credentials)){
            return redirect()->route('admin.home');
        }

        return redirect()->back()->withInput()->withErrors(['Dados informados podem estar incorretos']);
        
    }

    public function logout(){
        Auth::logout();
        return redirect()->route('login');
    }
}
