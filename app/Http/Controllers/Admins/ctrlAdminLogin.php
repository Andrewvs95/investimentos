<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Attempting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ctrlAdminLogin extends Controller
{
    public function __construct()
    {        
        $this->middleware('auth:admin');
    }

    public function loginForm() {
        return view('admin.loginForm');
    }

    public function login(Request $request){
        $this->validate($request, [
            'email' => 'required|email|max:255',
            'password' => 'required|max:255',
        ]);

        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
        ]; 

        if(Auth::guard('admin')->attempt($credentials)){
            return redirect()->intended(route('admin.home'));
        }

        return redirect()->back()->withInput($request->only('email'));                
    }

    public function logout(){
        Auth::logout();
        return redirect()->route('login');
    }
}
