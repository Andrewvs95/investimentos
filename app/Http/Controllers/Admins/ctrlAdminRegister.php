<?php

namespace App\Http\Controllers\Admins;

use App\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ctrlAdminRegister extends Controller
{

    public function registerForm() {    
    $check = Auth::guard('admin')->check();

        if($check === true){
            return view('admin.registerForm');
        }

    return redirect()->route('login');
        
    }

    public function register(Request $request) {

        $rule = [
            'name' => 'required|min:2|max:20',
            'email' => 'required|email|unique:users|max:255',
            'password' => 'required|max:255|confirmed',
        ];

        $message = [
            'name.required' => 'O campo nome não pode estra em branco.',
            'email.required' => 'O campo email não pode estra em branco',
            'password.required' => 'O campo senha não pode estra em branco',

            'name.min' => 'O campo precisa ter 2 caracteres no minimo.',
            
            'name.max' => 'O campo nome não pode conter mais que 20 caracteres.',
            'email.max' => 'O campo email não pode conter mais que 255 caracteres',
            'password.max' => 'O campo senha não pode conter mais que 255 caracteres',

            'email' => 'Verifique o campo e-mail, pois o mesmo contem um erro.',

            'unique' => 'Este e-mail já esta em uso.',

            'confirmed' => 'As senhas não coencidem.',
        ];

        $request->validate($rule,$message);

        $register = new Admin();
        $register->name = $request->input('name');
        $register->email = $request->input('email');
        $register->password = bcrypt($request->input('password'));
        $register->save();

        return redirect('/admin/home')->with('Status','Registro efetuado com sucesso');
    }
}
