<?php

namespace App\Http\Controllers\Bundles;

use App\Http\Controllers\Controller;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ctrlBundlePost extends Controller
{

    public function registerForm() {
        $check = Auth::guard('admin')->check();
    
            if($check === true){
                return view('bundle.registerForm');
            }
    
        return redirect()->route('login');     

    }

    public function register (Request $request) {

        $path = $request->file('post_file')->store('images','public'); 

        $post = new Post();
        $post->post_name = $request->input('post_name');
        $post->post_value = $request->input('post_value');        
        $post->post_quota = $request->input('post_quota');
        $post->post_file = $path; 
        $post->save();
        return redirect('/admin/home');
    }

    public function delete ($id) {
        $check = Auth::guard('admin')->check();
    
            if($check === true){
                $post = Post::find($id);

                if(isset($post)){
                    $post->delete();
                }
                
                return redirect('/admin/home');
            }
    
        return redirect()->route('login');    

    }

    public function editForm ($id) {
        $check = Auth::guard('admin')->check();
    
        if($check === true){
            $post = Post::find($id);
            if(isset($post)){
                return view('/bundle/editForm', compact('post') );
            }
            return redirect('/admin/home');
        }
        return redirect()->route('login');  

    }

    public function update (Request $request, $id) {
        
        $post = Post::find($id);

        $post->post_name = $request->input('post_name');
        $post->post_value = $request->input('post_value');
        $post->post_quota = $request->input('post_quota');

        if($request->hasFile('post_file')){
            $file = $request->file('post_file');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('storage', $filename,'public');
            $post->post_file = $filename;
        }

        $post->save();

        return redirect('/admin/home')->with('post', $post);

    }

}
