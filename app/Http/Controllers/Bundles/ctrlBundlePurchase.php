<?php

namespace App\Http\Controllers\Bundles;

use App\Http\Controllers\Controller;
use App\Post;
use App\User;
use App\post_user;
use App\Transaction;
use App\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ctrlBundlePurchase extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function purchase ($id) {
        $post = Post::find($id);  
        $uId = Auth::user()->id;
        
        $key = DB::table('post_users')    
            ->join('posts','post_users.post_id','=','posts.id')
            ->join('users','post_users.user_id','=','users.id')
            ->select('post_users.post_id','post_users.user_id')
            ->where([['users.id','=', $uId],['posts.id','=', $id],])
            ->get();  
    
        if(($post->post_quota > 0) and ($key == "[]")){
            if(isset($post)){
                return view('/bundle/purchase', compact('post') );
            }
        }

        return redirect('/escritorio/home')->with('message','Este curso já foi adquirido');
    }

    public function checkout (Request $request, $id) {

        $purchaseOK = false;
        
        $key = new post_user();   
        $post = Post::find($id);      
        $user = User::find(Auth::user()->id); 
        $wallet = Wallet::find(Auth::user()->id);        
        $historics = new Transaction();  
        

        $value = $post->post_value;
        $quota = $post->post_quota;
        $balance = $wallet->balance;

        if ($quota > 0) {
            $post->post_quota -= 1 ;
        } else {
            $purchaseOK = true;
            return redirect()->back();
        }

        if ($balance >= $value) {            
            $historics->user_id =  $user->id;
            $historics->balance_bf = $wallet->balance;
            $wallet->balance -= $post->post_value;
            $historics->balance = $wallet->balance;
            $historics->balance_desc = 'compra efetuada';
            $historics->date = $historics->update_at;
        } else {
            $purchaseOK = true;
            return redirect()->back()->with('Status','Saldo insulficiente');
        }
        
        if ($purchaseOK == false) {

            $key->user_id = $user->id;
            $key->post_id = $post->id;
            $key->bundle_key = true;

            $key->save();
            $post->save();
            $wallet->save();
            $user->save();
            $historics->save();

            return redirect('/escritorio/home')->with(['post', $post],['message','Compra efetuada com sucesso!']);
            
        }else{
            return redirect()->back();
        }

           
    }

    public function teste(){
        $id = 3;

            return DB::table('wallets')    
            ->join('users','wallets.user_id','=','users.id')
            ->where('wallets.user_id','=', $id)
            ->get();  

    }
}
