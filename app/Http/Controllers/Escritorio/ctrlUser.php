<?php

namespace App\Http\Controllers\Escritorio;

use App\Http\Controllers\Controller;
use App\Post;
use App\post_user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ctrlUser extends Controller
{
    public function index() {
        if(Auth::check() === true){
            $posts = Post::all();
            return view('escritorio.home', compact (['posts']) );
        }

        return redirect()->route('escritorio.login');
        
    }

    public function loginForm() {
        return view('escritorio.loginForm');
    }

    public function login(Request $request){
        if(!filter_var($request->email, FILTER_VALIDATE_EMAIL)){
            return redirect()->back()->withInput()->withErrors(['Email informado pode esta incorreto']);
        }

        $request->validate([
            'email' => 'required|email|max:255',
            'password' => 'required|max:255',
        ]);

        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
        ]; 

        if(Auth::attempt($credentials)){
            return redirect()->route('escritorio.home');
        }

        return redirect()->back()->withInput()->withErrors(['Dados informados podem estar incorretos']);
        
    }

    public function logout(){
        Auth::logout();
        return redirect()->route('escritorio.login');
    }
}
