<?php

namespace App\Http\Controllers\Escritorio;

use App\Http\Controllers\Controller;
use App\Transaction;
use App\User;
use App\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ctrlUserEdit extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function editForm(){       
       
        return view('/escritorio/editForm', array ('user' => Auth::user()));
    }

    public function update (Request $request) {
        
        $user = Auth::user();      

            $user->name = $request->input('name');
            $user->surname = $request->input('surname');
            $user->gender = $request->input('gender');          
            $user->save();       

        return view('/escritorio/editForm', array ('user' => Auth::user()))->with('message','Perfil atualizado com sucesso');
    }

    public function wallet(){
        
        $users = Auth::user()->id; 
        $historics = DB::table('transactions')->select('*')->where('user_id','=',$users)->get();
       
        return view('/escritorio/wallet', compact('historics'));                   
    }
}
