<?php

namespace App\Http\Controllers\Escritorio;

use App\Http\Controllers\Controller;
use App\Transaction;
use App\User;
use App\Wallet;
use Illuminate\Http\Request;

class ctrlUserRegister extends Controller
{
    public function registerForm() {
        return view('escritorio.registerForm');
    }

    public function register(Request $request) {

        $rule = [
            'name' => 'required|min:2|max:20',
            'surname' => 'min:2|max:20',
            'email' => 'required|email|unique:users|max:255',
            'password' => 'required|max:255|confirmed',
        ];

        $message = [
            'name.required' => 'O campo nome não pode estra em branco.',
            'surname.required' => 'O campo sobrenome não pode estra em branco.',
            'password.required' => 'O campo senha não pode estra em branco',

            'name.min' => 'O campo precisa ter 2 caracteres no minimo.',
            'surname.min' => 'O campo sobrenome precisa ter 2 caracteres no minimo.',

            'name.max' => 'O campo nome não pode conter mais que 20 caracteres.',
            'surname.max' => 'O campo sobrenome não pode conter mais que 20 caracteres.',
            'email.max' => 'O campo email não pode conter mais que 255 caracteres',
            'password.max' => 'O campo senha não pode conter mais que 255 caracteres',

            'email' => 'Verifique o campo e-mail, pois o mesmo contem um erro.',

            'unique' => 'Este e-mail já esta em uso.',

            'confirmed' => 'As senhas não coencidem.',
        ];

        $request->validate($rule,$message);        

        $register = new User();
        $register->name = $request->input('name');
        $register->surname = $request->input('surname');
        $register->email = $request->input('email');
        $register->gender = $request->input('gender');  
        $register->password = bcrypt($request->input('password'));
        $register->save();

        $wallet = new Wallet();
        $wallet->user_id =  $register->id;
        $wallet->balance += 10;
        $wallet->save();

        $historics = new Transaction();
        $historics->user_id =  $register->id;
        $historics->balance += $wallet->balance;
        $historics->balance_bf = 0; 
        $historics->balance_desc = 'credito adicionado';
        $historics->date = $historics->update_at;
        $historics->save();

        return redirect('/escritorio/login')->with('message','Você esta registrado, faça seu primeiro login!');
    }
}
