<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public function user(){
        return $this->belongsTo('App\User');
    }

    protected $fillable = [
    'user_id','balance','balance_bf','balance_desc','date',
    ];
}
