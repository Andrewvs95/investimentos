<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    public function post(){
        return $this->belongsToMany('App\Post');
    }

    public function historics(){
        return $this->hasMany('App\Transaction','user_id');
    }

    public function wallet(){
        return $this->hasOne('App\Wallet','user_id');
    }

    protected $fillable = [
        'id','name', 'surname', 'email', 'gender', 'password', 
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
