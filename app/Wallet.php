<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    public function user(){
        return $this->hasOne('App\User');
    }

    protected $fillable = [
        'user_id','balance',
    ];
}
