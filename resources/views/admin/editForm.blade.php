@extends('layout.appAdmin')

@section('body')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Editar usuario</div>        
                    <div class="container">
                        <form method="POST" action="{{route('account.update', $user->id)}}">
                          @csrf
                          <div class="form-group text-left">
                            <label for="name">Nome</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{$user->name}}">
                          </div>
                          <div class="form-group text-left">
                            <label for="surname">Sobrenome</label>
                            <input type="text" class="form-control" id="surname" name="surname" value="{{$user->surname}}">
                          </div>
                          <div class="form-group text-left">
                            <label for="email">E-mail</label>
                            <input type="text" class="form-control" id="email" name="email" value="{{$user->email}}">
                          </div>
                          <div class="form-group text-left">
                            <label for="wallet_balance">Adicionar credito</label>
                          <input type="number" class="form-control" id="wallet_balance" name="wallet_balance">
                          </div>
                          <h5>Genero</h5>
                          <div class="radio form-check-inline">
                            <label for="gender" class="radio-inline">
                            <input type="radio" name="gender" value="male">Masculino</label>
                          </div>
                          <div class="radio form-check-inline">
                            <label for="gender" class="radio-inline">
                            <input type="radio" name="gender" value="female">Feminino</label>
                          </div>
                          <div>
                            <a type="button" class="btn btn-danger" href="/admin/home">Cancelar</a>
                            <button type="submit" class="btn btn-primary my-2">Salvar</button>
                          </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


