@extends('layout.appAdmin')

@section('content')

<head>
  <title> Dashboard - administradores </title>
  
</head>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                                  
                Seja bem-vindo, a dashboard de administradores

                </div>

                <div class="card-body">
                  <div class="container">
                    <div class="row">
                      <div class="col-sm"> 
                        <a class="material-icons" href="/bundle/register">queue</a>
                      </div>
                      <div class="col-sm">
                        <a class="material-icons" href="/admin/register">person_add</a>
                      </div>
                    </div>
                  </div>

                  <!--Nav -->

                  <ul class="nav nav-tabs">
                    <li class="nav-item">
                      <a class="nav-link active" data-toggle="tab" href="#bundle">Gestão de pacotes</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" data-toggle="tab" href="#users">Gestão de usuarios</a>
                    </li>
                  </ul>

                  <!-- Conteudo do Nav -->

                  <div class="tab-content">

                    <!-- Pacotes -->

                    <div role="tabpanel" class="tab-pane active" id="bundle">
                      
                      <div class="album py-5 bg-light">
                      <div class="container">
                        <div class="row">
                          @foreach ($posts as $post)
                              
                              <div class="col-md-4">
                                <div class="card mb-4 shadow-sm">
                                  <img class="card-img-top figure-img img-fluid rounded" src="/storage/{{$post->post_file}}">
                                  <div class="card-body">
  
                                    <h4 class="card-text">{{$post->post_name}}</h4>
                                    
                                    <h5 class="card-text">Quantidade: {{$post->post_quota}}</h5>
                                  
                                    <p class="card-text">
                                      <h5>R$ {{$post->post_value}}</h5>
                                    </p>
                                            
                                    <div class="d-flex justify-content-between align-items-center">
                                      <div class="btn-group">
                                        <a type="button" class="btn btn-sm btn-outline-secondary" href="/bundle/editForm/{{$post->id}}">Editar</a>
                                        <form action="/bundle/delete/{{$post->id}}" action="post">
                                          @csrf
                                          <input type="hidden" name="_method" value="delete">
                                          <button type="submit" class="btn btn-sm btn-outline-danger">Apagar</button>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                          @endforeach
                        </div>
                      </div>
                    </div>

                  </div>

                  <!-- Usuarios -->

                    <div role="tabpanel" class="tab-pane" id="users">

                      <div class="album py-5 bg-light">
                        <div class="container">
                          <div class="row">
                                
                              <div class="card-body">
                                <table class="table table-hover">
                                  <thead>
                                    <tr>
                                      <th scope="col">Nome</th>
                                      <th scope="col">Sobrenome</th>
                                      <th scope="col">E-mail</th>                                   
                                      <th scope="col">Credito</th>
                                      <th scope="col">Genero</th>   
                                      <th scope="col" colspan="2">Ações</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    @foreach ($users as $user)
                                    <tr>
                                      <th>{{$user->name}}</th>
                                      <td>{{$user->surname}}</td>
                                      <td scope="row">{{$user->email}}</td>
                                      <td>{{$user->wallet->balance}}</td>
                                      <td>{{$user->gender}}</td>
                                      <td>
                                        <a type="button" class="btn btn-sm btn-outline-secondary" 
                                        href="/admin/editForm/{{$user->id}}">Editar</a>
                                      </td>
                                      <td>
                                        <form action="/admin/delete/{{$user->id}}" action="post">
                                          @csrf
                                          <input type="hidden" name="_method" value="delete">
                                          <button type="submit" class="btn btn-sm btn-outline-danger">Apagar</button>
                                        </form >
                                      </td>
                                    </tr>
                                    @endforeach
                                  </tbody>
                                </table>
                              </div>

                        </div>
                      </div>

                    </div>

                  </div>
                   
                </div>
            </div>
        </div>
    </div>
</div>

    <footer class="text-muted">
      <div class="container">
        <p class="float-right">
          <a href="#">Voltar para cima</a>
        </p>
      </div>
    </footer>

 
@endsection
