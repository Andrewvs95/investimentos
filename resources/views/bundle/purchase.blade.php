@extends('layout.app')

@section('body')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Comprar pacotes</div>                    
                    <div class="container">
                      <label for="wallet_balance">
                        Seu saldo: R$ {{ Auth::user()->wallet->balance }}
                      </label>  
                        <form method="POST" action="{{route('bundle.checkout', $post->id)}}" enctype="multipart/form-data">
                          @csrf
                          <div class="custom-file">
                            <img class="card-img-top figure-img img-fluid rounded" 
                            style="max-width:50%;height:auto;margin-left:25%;margin-top:10px;"
                            src="/storage/{{$post->post_file}}">
                            
                            <div class="row">
                              <div class="col-sm">
                                <div class="form-group text-left">
                                  <label for="post_name">Curso: <h4>{{$post->post_name}}</h4></label>
                                </div>
                              </div>
                              <div class="col-sm">
                                <div class="form-group text-left">
                                  <label for="post_value">Preço: <h4>R$ {{$post->post_value}}</h4></label>
                                </div>
                              </div>
                              <div class="col-sm">
                                <div class="form-group text-left">
                                  <label for="post_quota">Restam: <h4>{{$post->post_quota}}</h4></label>
                                </div>
                              </div>                              
                            </div>
                            <div class="form-group text-left">
                              <a type="button" class="btn btn-danger" href="/escritorio/home">Cancelar</a>
                              <button type="submit" class="btn btn-primary my-2">Comprar</button>
                            </div>
                          </div>
                        </form>
                    </div>
                </div>
            </div>
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
          @endif
        </div>
    </div>
</div>
@endsection


