@extends('layout.appAdmin')

@section('body')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Criar pacotes</div>        
                    <div class="container">
                        <form method="POST" action="{{route('bundle.register.do')}}" enctype="multipart/form-data">
                          @csrf
                          <div class="form-group text-left">
                            <label for="post_name">Nome do pacote</label>
                            <input type="text" class="form-control" id="post_name" name="post_name">
                          </div>
                          <div class="form-group text-left">
                            <label for="post_value">Valor do pacote</label>
                            <input type="number" class="form-control" id="post_value" name="post_value" data-type="currency">
                          </div>
                          <div class="form-group text-left">
                            <label for="post_quota">Quantidade de pacotes</label>
                            <input type="number" class="form-control" id="post_quota" name="post_quota">
                          </div>
                          <div class="custom-file">
                            <input type="file" class="custom-file-input" id="post_file" name="post_file">
                            <label class="custom-file-label" for="post_file">Escolha um arquivo</label>
                          </div>
                            <button type="submit" class="btn btn-primary my-2">Salvar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


