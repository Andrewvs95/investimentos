@extends('layout.app')

@section('body')
@if(session()->has('message'))
	<div class="alert alert-info" role="alert">
		{{session()->get('message')}} 
	</div>
@endif
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Editar usuario</div>        
                    <div class="container">
                        <form method="POST" action="{{route('escritorio.update', $user->id)}}">
                          @csrf
                          <div class="form-group text-left">
                            <label for="name">Nome</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{$user->name}}">
                          </div>
                          <div class="form-group text-left">
                            <label for="surname">Sobrenome</label>
                            <input type="text" class="form-control" id="surname" name="surname" value="{{$user->surname}}">
                          </div>
                          <div class="form-group text-left">
                            <label for="email">E-mail</label>
                            <input type="text" class="form-control" id="email" name="email" value="{{$user->email}}" readonly>
                          </div>

                          <h5>Genero</h5>
                          <div class="radio form-check-inline">
                            <label for="gender" class="radio-inline">
                            <input type="radio" name="gender" value="male">Masculino</label>
                          </div>
                          <div class="radio form-check-inline">
                            <label for="gender" class="radio-inline">
                            <input type="radio" name="gender" value="female">Feminino</label>
                          </div>
                          
                          <div>
                            <a type="button" class="btn btn-danger" href="/escritorio/home">Cancelar</a>
                            <button type="submit" class="btn btn-primary my-2">Salvar</button>
                          </div>
                          
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


