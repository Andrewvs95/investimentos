@extends('layout.app')

@section('content')
@if(session()->has('message'))
	<div class="alert alert-info" role="alert">
		{{session()->get('message')}}
	</div>
@endif

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                                  
                Seja bem-vindo, {{ Auth::user()->name }}

                </div>

                <div class="card-body">
                  <div class="container">
                    <div class="row">
                      <div class="col-sm">
                        <a class="material-icons" href="/escritorio/editForm">account_circle</a>
                      </div>
                      <div class="col-sm">
                        <a class="material-icons" href="/escritorio/wallet">account_balance_wallet</a>
                          R$:{{ Auth::user()->wallet->balance}}
                     </div>
                    </div>
                  </div>
                  

                <div class="album py-5 bg-light">
                    <div class="container">
                      <div class="row">
                        @foreach ($posts as $post)
                          @if ($post->post_quota > 0)

                          <div class="col-md-4">
                            <div class="card mb-4 shadow-sm">
                              <img class="card-img-top figure-img img-fluid rounded" src="/storage/{{$post->post_file}}">
                              <div class="card-body">

                                <h4 class="card-text">{{$post->post_name}}</h4>

                                <h5 class="card-text">Pacotes restantes: {{$post->post_quota}}</h5>
                                
                                <p class="card-text">
                                  <h4>R$ {{$post->post_value}}</h4>
                                </p>
                                                                 
                                <div class="d-flex justify-content-between align-items-center">
                                  @if ($post->user() == false)
                                  <div class="btn-group">
                                    <h5>Produto adquirido</h5>
                                  </div>
                                  @else
                                  <div class="btn-group">
                                    <a type="button" class="btn btn-sm btn-outline-success"
                                    href="/bundle/purchase/{{$post->id}}">Comprar</a> 
                                  </div>
                                  @endif
                                  
                                  <div>
                                    
                                  </div>
                                </div>

                              </div>
                            </div>
                          </div>
          
                          @endif 
                        @endforeach
                      </div>
                    </div>
                  </div>



                    
                </div>
            </div>
        </div>
    </div>
</div>

    <footer class="text-muted">
      <div class="container">
        <p class="float-right">
          <a href="#">Voltar para cima</a>
        </p>
      </div>
    </footer>

 
@endsection
