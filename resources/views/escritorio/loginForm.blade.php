@extends('layout.app')

@section('body')
@if(session()->has('message'))
	<div class="alert alert-info" role="alert">
		{{session()->get('message')}}
	</div>
@endif
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-6">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title" id="basic-layout-colored-form-control">Efetuar login</h4>
					<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
				</div>
				<div class="card-content collapse show">
					<div class="card-body">

                    <form class="form" method="POST" action="{{route('escritorio.login.do')}}">
						@csrf
							<div class="form-body">

							@if ($errors->all())
								@foreach ($errors->all() as $error)
									<div class="alert alert-danger" role="alert">
										{{$error}}
									</div>
								@endforeach
							@endif
								
									<div class="row">
										<div class="col-md-9">
											<div class="form-group">

													<div class="form-group">
														<input type="text" class="form-control col border-primary "
														name="email" id="email" placeholder="E-mail" value="{{ old('email)') }}"
														required autocomplete="email" autofocus>				
													</div>
												
													<div class="form-group">
														<input type="password" class="form-control border-primary"
														name="password" id="password" placeholder="Senha" required> 				
													</div>

											</div>
										</div>
									</div>

							</div>
							
							<div class="form-actions right">
								<button type="submit" class="btn btn-primary">
									{{ __('Login') }}
								</button>
							</div>
						</form>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>   

@endsection


