@extends('layout.app')

@section('body')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-6">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title" id="basic-layout-colored-form-control">Criar um perfil</h4>
                </div>
                
				<div class="card-content collapse show">
					<div class="card-body">

            <form class="form" method="post" action="{{route('escritorio.register.do')}}">
              @csrf
				<div class="form-body">
					<h4 class="form-section">Sobre você</h4>
						<div class="row">

							<div class="col-md-6">
								<div class="form-group">
									  	<input type="text" class="form-control col border-primary 
									  	{{ $errors -> first('name')? 'is-inavlid' : ''}}" name="name" id="name" placeholder="Nome">
                      					@if ($errors->first('name'))
                        					<div class="alert alert-danger" role="alert">
                          						{{$errors->first('name')}}
                        					</div>
                      					@endif
								</div>
                  			</div>
                  
							<div class="col-md-6">
								<div class="form-group">
										<input type="text" class="form-control col border-primary 
										{{ $errors->has('surname')? 'is-inavlid' : ''}}" name="surname" id="surname" placeholder="Sobrenome">
                     					@if ($errors->has('surname'))
                        					<div class="alert alert-danger" role="alert">
												{{$errors->first('surname')}}
                        					</div>
                      					@endif
								</div>
                            </div>
                            
						</div>

							<div class="form-group">
								  <input type="text" class="form-control border-primary 
								  {{$errors->has('email')? 'is-inavlid' : ''}}" name="email" id="email" placeholder="Email">
                  					@if ($errors->has('email'))
                    					<div class="alert alert-danger" role="alert">
                      						{{$errors->first('email')}}
                    					</div>
                  					@endif
                			</div>
                
                <div class="form-group">
					<h5>Genero</h5>
					<div class="radio form-check-inline">
						<label for="gender" class="radio-inline">
						<input type="radio" name="gender" value="male">Masculino</label>
					</div>
					<div class="radio form-check-inline">
						<label for="gender" class="radio-inline">
						<input type="radio" name="gender" value="female">Feminino</label>
					</div>
								
					@if ($errors->has('gender'))
						<div class="alert alert-danger" role="alert">
							{{$errors->first('gender')}}
						</div>
					@endif
				</div>

				<div class="form-group">
					<input type="password" class="form-control border-primary {{$errors->has('password')? 'is-inavlid' : ''}}" name="password" 
					id="password" placeholder="Nova senha" required>    
					@if ($errors->has('password'))
						<div class="alert alert-danger" role="alert">
						 	{{$errors->first('password')}}
						</div>
				 	@endif               
				</div>

				<div class="form-group">
                    <input type="password" class="form-control border-primary" name="password_confirmation" id="password_confirmation" placeholder="Repetir senha" required>                   
				</div>      
				
				<div class="form-actions right">
					<button type="submit" class="btn btn-primary">
						<i class="fa fa-check-square-o"></i> Cadastrar
					</button>
				</div>
				</form>

					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</div>   
@endsection


