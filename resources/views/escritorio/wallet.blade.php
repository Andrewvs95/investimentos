@extends('layout.app')

@section('body')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Sua carteira</div>        
                    <div class="container">
                        <table class="table table-hover">
                            <thead>
                              <tr>
                                <th scope="col">Saldo</th>
                                <th scope="col">Saldo anterior</th>
                                <th scope="col">Atividades</th>
                                <th scope="col">Data</th>                                   
                              </tr>
                            </thead>
                            <tbody>
                              @foreach ($historics as $hist)
                              <tr>
                                <th>{{$hist->balance}}</th>
                                <th>{{$hist->balance_bf}}</th>
                                <td>{{$hist->balance_desc}}</td>
                                <td>{{ date('j M Y H:i', strtotime($hist->updated_at)) }}</td>
                              </tr>
                              @endforeach
                            </tbody>
                          </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


