<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Investimentos.com</title>

    <script src="{{ asset('js/app.js') }}" defer></script>

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Baloo+Paaji+2:wght@400;500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="style.css"/>   

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">                
                @guest                
                    <a class="navbar-brand" href="{{ url('/') }}">
                        Investimentos.com
                    </a>
                    
                @else
                
                <a class="navbar-brand" href="{{ url('/escritorio/home') }}">
                    Home
                </a>      
    
                @endguest

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <ul class="navbar-nav ml-auto">
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('escritorio.login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('escritorio.register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('escritorio.register') }}">{{ __('Registrar') }}</a>
                                </li>
                            @endif
                        @else
                                
                        <li class="nav-item">
                            <a class="vav-link" href="{{route('escritorio.logout')}}">{{__('Sair')}}</a>
                        </li>

                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <main role="main">
        @hasSection ('body')
            @yield('body')             
        @endif
    <script src="{{asset ('js/app.js')}}" type="text/javascript"></script>

    @hasSection ('javascript')
        @yield('javascript')
    @endif
</body>
</html>
