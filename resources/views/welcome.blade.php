<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Investimentos.com</title>

        <link href="https://fonts.googleapis.com/css2?family=Baloo+Paaji+2:wght@400;500&display=swap" rel="stylesheet">

        <style>
            html, body {
                padding: 20px;
                background-color: #fff;
                color: #636b6f;
                font-family: 'Baloo Paaji 2', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 200;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
                    
            @if (Route::has('escritorio.login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/escritorio/home') }}">Home</a>
            @else
                        <a href="{{ route('escritorio.login') }}">Login</a>

                        <a href="{{ route('login') }}">Admin</a>

                        @if (Route::has('escritorio.register'))
                            <a href="{{ route('escritorio.register') }}">Registrar</a>
                        @endif
                    @endauth
                </div>
            @endif
            
            

            <div class="content">
                
            </div>
        </div>
    </body>
</html>
