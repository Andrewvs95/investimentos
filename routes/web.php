<?php

use App\Http\Controllers\ctrlPost;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Seção de pacotes  
Route::prefix('bundle')->group(function(){
    Route::get('/register', 'Bundles\ctrlBundlePost@registerForm')->name('bundle.register');
    Route::post('/register/do', 'Bundles\ctrlBundlePost@register')->name('bundle.register.do');

    Route::get('/editForm/{id}', 'Bundles\ctrlBundlePost@editForm');
    Route::post('/update/{id}', 'Bundles\ctrlBundlePost@update')->name('bundle.update');
    
    Route::get('/delete/{id}', 'Bundles\ctrlBundlePost@delete')->name('delete');

    Route::get('/purchase/{id}', 'Bundles\ctrlBundlePurchase@purchase');
    Route::post('/checkout/{id}', 'Bundles\ctrlBundlePurchase@checkout')->name('bundle.checkout');
});

//Seção de usuario
Route::prefix('escritorio')->group(function(){
    Route::get('/home', 'Escritorio\ctrlUser@index')->name('escritorio.home');

    Route::get('/login', 'Escritorio\ctrlUser@loginForm')->name('escritorio.login');
    Route::post('/login/do', 'Escritorio\ctrlUser@login')->name('escritorio.login.do');
    Route::get('/logout', 'Escritorio\ctrlUser@logout')->name('escritorio.logout');

    Route::get('/register','Escritorio\ctrlUserRegister@registerForm')->name('escritorio.register');
    Route::post('/register/do','Escritorio\ctrlUserRegister@register')->name('escritorio.register.do');
    
    Route::get('/editForm', 'Escritorio\ctrlUserEdit@editForm');
    Route::post('/update', 'Escritorio\ctrlUserEdit@update')->name('escritorio.update');

    Route::get('/wallet', 'Escritorio\ctrlUserEdit@wallet')->name('escritorio.wallet');
});

//Seção de administrador
Route::prefix('admin')->group(function(){
    Route::get('/home', 'Admins\ctrlAdmin@index')->name('admin.home');
    
    Route::get('/login', 'Admins\ctrlAdmin@loginForm')->name('login');
    Route::post('/login/do', 'Admins\ctrlAdmin@login')->name('admin.login.do');
    Route::get('/logout', 'Admins\ctrlAdmin@logout')->name('admin.logout');
    
    Route::get('/register','Admins\ctrlAdminRegister@registerForm')->name('admin.register');
    Route::post('/register/do','Admins\ctrlAdminRegister@register')->name('admin.register.do');

    //Edição de usuarios
    Route::get('/editForm/{id}', 'Admins\ctrlAccountEdit@editForm');
    Route::post('/update/{id}', 'Admins\ctrlAccountEdit@update')->name('account.update');
    
    Route::get('/delete/{id}', 'Admins\ctrlAccountEdit@delete')->name('account.delete');
});

Route::get('/teste','Bundles\ctrlBundlePurchase@teste');

